package pl.ssolutions;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Created by Artur Buczek on 06.01.2018.
 */
public class AmazingListTest {

    @Test
    public void size() {
        assertEquals(0, new AmazingList<Integer>().size());
        assertEquals(3, new AmazingList<Integer>(1, 2, 3).size());
    }

    @Test
    public void equals() {
        assertTrue(new AmazingList<>(1, 2, 3, 4).equals(new AmazingList<>(1, 2, 3, 4)));
    }

    @Test
    public void get() {
        try {
            new AmazingList<Integer>().get(10);
            fail("It should cause an Exception!");
        } catch (IndexOutOfBoundsException ex) {}

        try {
            new AmazingList<Integer>(1, 2, 3).get(-1);
            fail("It should cause an Exception!");
        } catch (IndexOutOfBoundsException ex) {}

        assertEquals(2, new AmazingList<>(1, 2, 3, 4).get(1).intValue());
    }

    @Test
    public void reverse() {
        assertTrue(
                new AmazingList<>(1, 2, 3, 4, 5, 6).reverse().equals(new AmazingList<>(6, 5, 4, 3, 2, 1))
        );

        assertTrue(
                new AmazingList<>("one", "two", "three", "four").reverse()
                        .equals(new AmazingList<>("four", "three", "two", "one"))
        );
    }

    @Test
    public void filter() {
        assertTrue(new AmazingList<>(1, 2, 3, 4, 5, 6).filter(el -> el % 2 == 0).equals(new AmazingList<>(2, 4, 6)));

        AmazingList<Integer> filtered = new AmazingList<Integer>().filter(el -> el % 2 == 0);
        assertEquals(0, filtered.size());
    }

    @Test
    public void map() {
        assertTrue(new AmazingList<>(1, 2, 3, 4, 5, 6).map(el -> el * 2).equals(new AmazingList<>(2, 4, 6, 8, 10, 12)));
    }

    @Test
    public void foldLeft() {
        String concatenated = new AmazingList<>("H", "e", "l", "l", "o", " ", "W", "o", "r", "l", "d")
                .foldLeft("", (col, el) -> col + el);
        assertEquals("Hello World", concatenated);
    }
}
