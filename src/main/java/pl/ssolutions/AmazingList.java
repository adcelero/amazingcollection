package pl.ssolutions;

import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * Immutable list with the most basic functional operations
 *
 * Created by Artur Buczek on 06.01.2018
 */
public class AmazingList<T> {

    private T[] collection;

    public AmazingList(T... elements) {
        collection = elements;
    }

    /**
     * Returns the size of the list
     *
     * @return size of the list
     */
    public int size() {
        return collection.length;
    }

    /**
     * complexity O(1)
     *
     * Returns an instance of an Object at given index.<br/>
     * First index in the list = 0<br/>
     * Last index in the list = size of the list - 1
     *
     * @param index
     * @return
     * @throws IndexOutOfBoundsException
     */
    public T get(int index) throws IndexOutOfBoundsException {
        if (index < 0 || index >= collection.length) throw new IndexOutOfBoundsException("There is no element at " + index);
        else return collection[index];
    }

    /**
     * complexity O(1)
     *
     * Checks whether the list is empty
     *
     * @return true if the list is empty
     */
    public boolean isEmpty() {
        return collection.length == 0;
    }

    /**
     * complexity O(n)
     *
     * Check whether given list contains the same objects, with the same ordering
     *
     * @param list the list with which to compare
     * @return true - if the given list contents are equal
     */
    public boolean equals(AmazingList<T> list) {
        if (list.size() != this.size()) return false;

        for (int idx = 0;idx < this.size(); idx += 1) {
            if (!list.get(idx).equals(this.get(idx))) return false;
        }

        return true;
    }

    /**
     * complexity O(n)
     * Performs the exact reversed version of the list so that the first element
     * becomes the last, the second - second from the end and so forth.
     *
     * @return reversed version of the list
     */
    public AmazingList<T> reverse() {
        if (isEmpty()) return new AmazingList<T>(collection.clone());

        T[] duplicate = collection.clone();

        int dupIdx = 0;
        for (int collIdx = collection.length - 1; collIdx >= 0; collIdx -= 1) {
            duplicate[dupIdx++] = collection[collIdx];
        }

        return new AmazingList<T>(duplicate);
    }


    /**
     * complexity O(n)
     * Filters the list by examing each element with the given clojure.
     * The closure should perform the test on the given element on the list and give boolean in return.
     *
     * @param block closure with an element on the list on input and boolean on output
     * @return filtered instance of the list
     */
    public AmazingList<T> filter(Function<T, Boolean> block) {
        if (isEmpty()) return new AmazingList<T>(collection.clone());

        T[] duplicate = collection.clone();

        int removed = 0;
        for (int idx = 0;idx < collection.length; idx += 1) {
            if (!block.apply(collection[idx])) {
                duplicate[idx] = null;
                removed += 1;
            }
        }

        T[] filtered = (T[]) new Object[collection.length - removed];
        int idx = 0;
        for (T element: duplicate) {
            if (element != null) {
                filtered[idx] = element;
                idx += 1;
            }
        }

        return new AmazingList<T>(filtered);
    }

    /**
     * complexity 0(n)
     * Maps the elements of the list with the closure. The closure should take an element of the list as input
     * and give the resultant object in return.
     *
     * @param block closure which map each element of the list into another object
     * @param <R>
     * @return an instance of the list with mapped elements
     */
    public <R> AmazingList<R> map(Function <T, R> block) {
        if (isEmpty()) return new AmazingList<>((R[]) new Object[0]);

        R[] converted = (R[]) new Object[collection.length];

        for (int elIdx = 0;elIdx < collection.length;elIdx += 1) {
            converted[elIdx] = block.apply(collection[elIdx]);
        }

        return new AmazingList<>(converted);
    }

    /**
     * complexity O(n)
     * Concatenates the elements of the list starting with the inital object initVal with a rule given by the closure.
     * The closure should take two parameters:
     * - the first one - an object into which all of the elements are concatenated
     * - the second one - an element of the list to concatenate with the object
     *
     * @param initVal object into which elements of the list will be concatenated
     * @param block closure which performs the concatenation on a single element of the list
     * @param <R>
     * @return whole list folded into an object with the same type as initVal
     */
    public <R> R foldLeft(R initVal, BiFunction<R, T, R> block) {
        R concatenated = block.apply(initVal, collection[0]);

        for (int idx = 1;idx < collection.length; idx += 1) {
            concatenated = block.apply(concatenated, collection[idx]);
        }

        return concatenated;
    }
}
